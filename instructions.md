#NOTE

Under normal circumstances the connection information would not be made available on here, this is just local credentials.

---

Install Docker Desktop

Sign Up then sign in

Open terminal, navigate to project folder and run:

<code>docker-compose up -d</code>

Navigate to your preferred Database Manager (I'm using Beekeeper Studio).

Connect using the credentials found in the .env file

After the connection is succesful run the queries found main.sql

You can now access the website via [this link](locahost) or [this one](127.0.0.1)
