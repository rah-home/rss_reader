-- CREATE LINKS TABLE 
CREATE TABLE IF NOT EXISTS links(
  id INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(100),
  url VARCHAR(300) NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- INSERT SINGLE ROW
INSERT INTO links (name, url) VALUES ("BBC", "http://feeds.bbci.co.uk/news/rss.xml?edition=uk");