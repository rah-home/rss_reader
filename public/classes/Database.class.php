<?php

class Database extends PDO
{
    protected $host = 'database';
    protected $database = 'rss_reader';
    protected $username = 'root';
    protected $password = 'asdf';

    /**
       * On instiate, try to connect to DB
    */
    public function __construct()
    {
        try {
            parent::__construct("mysql:host=$this->host;dbname=$this->database", $this->username, $this->password);

        } catch(PDOException $e) {
            die("Connection failed: " . $e->getMessage());
        }
    }

}