<?php

include 'Database.class.php';

class Link 
{
    public $conn;

    /**
       * Call DB connection
    */
    public function __construct()
    {
        $this->conn = new Database();
    }

    /**
       * Fetch rss link(s) from DB
       * @param int $id 
       * @return array $links
    */
    public function fetch($id = 0) 
    {
        $where = '';
        if ((int) $id > 0) {
            $where = ' WHERE `id` = ' . $id;
        }

        try {
            $sth = $this->conn->prepare('SELECT `id`, `name`, `url` FROM `links` ' . $where);
            $sth->execute();
            $links = $sth->fetchAll(PDO::FETCH_ASSOC);
        } catch(PDOException $e) {
            $links = [];
        }
        
        return $links;
    }

    /**
       * Inserts url into links table return fail or new link id
       * @param string $url 
       * @return int $response
    */
    public function add($url)
    {
        $response = 0;

        // If url exists (not null value) then attempt insert
        if (!empty($url)) {

            try {
                $sth = $this->conn->prepare('INSERT INTO `links` (`url`) VALUES (?)');
                $sth->execute([$url]);

                if ((int) $this->conn->lastInsertId() > 0) {
                    $response = $this->conn->lastInsertId();
                }
            } catch(PDOException $e) {
                $response = 0;
            }
        }

        echo json_encode($response); exit;

    }

    /**
       * Updates DB row based on link id and values
       * @param int $id 
       * @param array $values
       * @return int $response
    */
    public function update($id, $values = []) 
    {
        $response = 0;

        if ((int) $id > 0 || !empty($values)) {

            try {
                $sth = $this->conn->prepare('UPDATE `links` SET `name` = ?, `url` = ? WHERE `id` = ?');
                $sth->execute([$values['name'], $values['url'], $id]);
                $response = 1;
            } catch(PDOException $e) {
                $response = 0;
            }
            
        }
        
        echo json_encode($response); exit;

    }

    /**
       * Delete rss record from links based on id
       * @param int $id 
       * @return int $response
    */
    public function purge($id) 
    {
        $response = 0;

        if ((int) $id > 0) {

            try {
                $sth = $this->conn->prepare('DELETE FROM `links` WHERE `id` = ?');
                $sth->execute([$id]);
                $response = 1;
            } catch(PDOException $e) {
                $response = 0;
            }

        }

        echo json_encode($response); exit;
        
    }

    public function dd($var) 
    { 
        echo '<pre>', var_dump($var), '</pre>';
        die();
    }
}