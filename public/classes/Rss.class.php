<?php

class Rss 
{
    public $feed;
    public $url;

    /**
       * Parse RSS link and return array of feed
       * @param string $url 
       * @return mixed $response
    */
    public function fetch($url) 
    {
        $response = 0;
        
        $valid = $this->validate($url);

        if ($valid == 'true') {
            $this->url = $url;
            $this->feed = new SimpleXMLElement($url, LIBXML_NOCDATA, true);
            $this->feed = json_decode(json_encode($this->feed), TRUE);
    
            if (!empty($this->feed)) {
                $response = $this->filterXml();
            }
        }
        
        return $response;
    }

    /**
     * Check if url is RSS valid
     * @param string $url
     * @return string $result
    */
    public function validate($url)
    {
        $contents = file_get_contents("http://validator.w3.org/feed/check.cgi?url=$url&output=soap12");
        $a = strpos($contents, '<m:validity>', 0)+12; 
        $b = strpos($contents, '</m:validity>', $a); 
        $result = substr($contents, $a, $b-$a); 
        
        return $result;
    }

    /**
       * Filter the array of XML content
       * @return array $array
    */
    private function filterXml()
    {

        $channel = $this->feed['channel'];

        if (empty($channel['item'])) {
            $channel = $this->feed;
        }

        $array = [
            'title' => $channel['title'] ?: $channel['channel']['title'],
            'description' => $channel['description'],
            'image' => $channel['image']['url'],
            'articles' => $this->filterItems($channel['item']),
            'source' => $this->url
        ];

        return $array;
        
    }

    /**
       * Filter each article
       * @param array $items 
       * @return array $articles
    */
    private function filterItems($items)
    {
        $articles = [];
        foreach ($items as $item) {
            $articles[] = [
                'title' => $item['title'],
                'link' => $item['link'],
                'description' => substr($item['description'], 0, 150) . '...',
                'date' => $item['pubDate']
            ];
        }
        return $articles;
    }

    public function dd($var) 
    { 
        echo '<pre>', var_dump($var), '</pre>';
        die();
    }
}