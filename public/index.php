<?php 

require '../vendor/autoload.php';
include 'classes/Link.class.php';
include 'classes/Rss.class.php';

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

$loader = new FilesystemLoader(__DIR__.'/templates');
$twig = new Environment($loader);

// Create instances of links and rss
$links = new Link();
$rss = new Rss();

// If type and id are set then delete or update based on type else check for add type with url
if (!empty($_POST['type'])) {
    if (!empty($_POST['url']) && $_POST['type'] === 'add') {
        if ((bool) $rss->validate($_POST['url']) === true) {
            $links->add($_POST['url']);
        } else {
            $data['feed']['message'] = 'Invalid RSS';
        }
    }
    if((!empty($_POST['id']) && (int) $_POST['id'] > 0)) {
        if ($_POST['type'] === 'delete') {
            $links->purge($_POST['id']);
        }
        if ($_POST['type'] === 'update' && !empty($_POST['url']) && !empty($_POST['name'])) {
            $links->update($_POST['id'], ['url' => $_POST['url'], 'name' => $_POST['name']]);
        }
    }
}

// If ID given, fetch details and rss feed
if (!empty($_GET['id']) && (int) $_GET['id']) {
    $link = $links->fetch($_GET['id']);
    $feed = $rss->fetch($link[0]['url']);

    if (is_array($feed)) {
        $data['feed'] = $feed;
    } else {
        $data['feed']['message'] = 'Invalid RSS';
    }
    $data['feed']['id'] = $_GET['id'];
}

// Insert the minified css in style tags for page speed
$data['minified_css'] = file_get_contents(__DIR__.'/assets/style.min.css');
$data['sidebar_links'] = $links->fetch();

// print_r($data);die;
echo $twig->render('main.twig', $data);